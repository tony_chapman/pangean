<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>

		<header><h1 class="page-title screen-reader-text"><?php echo pilot_get_title(); ?></h1></header>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', 'posts' ); ?>
		<?php endwhile; ?>
		<?php the_posts_navigation(); ?>
		<?php else : ?>
			<?php get_template_part( 'views/content', 'none' ); ?>
		<?php endif; ?>

<?php get_footer(); ?>