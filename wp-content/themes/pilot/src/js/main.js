(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();
jQuery(document).ready(function($) {
	$('.menu-toggle').on('click touchdown', function () {
    $('html, body').animate({
        scrollTop: $("#menu-footer").offset().top
    }, 1000);
	});
});