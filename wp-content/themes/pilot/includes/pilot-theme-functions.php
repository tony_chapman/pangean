<?php
	function pilot_get_title(){
		if ( is_front_page() && is_home() ) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif ( is_front_page() ) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif ( is_home() ) {
		  // blog page
			return "News";
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	function excerpt($limit) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).' ...';
		} else {
			$excerpt = implode(" ",$excerpt);
		} 
		$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
		return $excerpt;
	}
?>