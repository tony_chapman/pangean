<?php
	require get_template_directory() . '/includes/modules/additional_acf_field_groups.php';

	global $pilot;

	$pilot->module_names = array();
	$pilot->modules = get_modules();
	$pilot->modules = array_merge( $pilot->modules, $pilot->additional_modules); // $pilot->additional_modules are declared in functions.php
	$pilot->layouts = array();		// will load acf layouts for modules

	foreach( $pilot->modules as $module ){
		$module_layout = array();
		$filename = get_template_directory() . '/includes/modules/' . $module . '/functions.php';
		// module's acf layout is built and added to $pilot->layouts[] in the following require
		if( file_exists ( $filename )){
			require $filename;
			$additional_fields = get_additional_fields( $module );
			foreach( $additional_fields as $field ){
				$module_layout['sub_fields'][] = $field;
			}
			$pilot->layouts[] = $module_layout;
			$pilot->module_names[] = array(
				'class' => preg_replace('/_/','-',$module),
				'key' => $module_layout['key'],
				'func_name' => preg_replace('/-/','_',$module),
				'dir_name' => $module
			);
		}
	}
	// include global module builder
	if( $pilot->use_global_modules ){
		require get_template_directory() . '/includes/modules/global_acf_def.php';
		if( ( array_key_exists( 'page', $_GET ) && $_GET['page'] != 'global-modules' ) || !array_key_exists( 'page', $_GET ) ){
			$pilot->layouts[] = $global_module_layout;
		}
	}
	// all layouts are added to the $pilot->flexible_content layouts key in the following require
	require get_template_directory() . '/includes/modules/flexible_content_acf_def.php';
	acf_add_local_field_group( $pilot->flexible_content );

	/* ACF Custom Functions */
	function get_all_blocks( $content_block_name = 'content_blocks' , $is_option = false){
		$option = "";
		if( $is_option ){
			$option = 'option';
		}
		if( have_rows($content_block_name , $option ) ):
			$double_media_args = array();
			global $i;
			$i = 0;
			while ( have_rows($content_block_name , $option) ): 
				the_row();
				$block_type = preg_replace( '/_block/','',get_row_layout());
				$params = array('type'=>$block_type);
				global $args;
				$args = array();
				$custom_classes = "";
				$color_class = " ";
				// checks if colormaker is loaded
				if( function_exists('get_color_class') ){ 
					if( $block_color_key = get_sub_field($block_type.'_block_color') ){
						// checks all colors as defined in colormaker settings and matches to hidden id
						$block_color = get_color_class( $block_color_key );
						$color_class .= $block_color . " module";
					}
				}
				if( $block_custom_classes = get_sub_field('custom_classes_'.$block_type) ){
					$custom_classes .= $block_custom_classes;
				}
				$args = call_user_func('build_' . $block_type . '_layout');
				if( 'global' == $block_type ){
					$global_type = get_sub_field('global_block_predefined_block');
					$params['type'] = $args['global_type'];
				}				
				if( count( $args ) > 0 ){
					if( $block_type != 'media' ){
						$args['id'] = $block_type.'_block_'.$i;
					}
					$params['args'] = $args;
					$params['classes'] = $custom_classes . $color_class;
					get_block( $params );
				}
				$i++;
			endwhile;
		endif;	
	}
				
	function get_block( $params ){
		global $pilot;
		if( $block_type = $params['type'] ){
			if( array_key_exists( 'classes', $params ) ){
				$custom_classes = $params['classes'];
			}
			$args = @$params['args'];
			$id = "";
			if( is_array( $args ) && array_key_exists( 'id', $args ) ){
				$id = "id='".$args['id']."'";
			}
			$block_class = preg_replace('/_/','-',$block_type);
			$block_dir_name = get_dir_name_from_class($block_class);
			echo "<div class='block-".$block_type . $pilot->module_classes ." ". $custom_classes ."' ". $id . "><div class='layout-content'>";	
 				get_template_part( 'includes/modules/' . $block_dir_name . '/module', 'view' );
			echo "</div><!--/layout-content--></div><!--/block-->";
			unset($args);
		}
	}
	function get_modules(){
		$current_directory = __FILE__;
		$root = preg_replace( '#includes(.*?)modules(.*?)modules.php#', '', $current_directory);
		$path = $root . "/includes/modules";
		$dirs = glob($path . '/*' , GLOB_ONLYDIR);
		$modules = array();
		foreach( $dirs as $dir ){
			$pos = strrpos($dir, '/') + 1;
			$module = substr($dir,$pos);
			if( 'module-starter' != $module ){
				$modules[] = $module;
			}
		}
		return $modules;
	}
	function get_dir_name_from_class($class_name){
		global $pilot;
		foreach( $pilot->module_names as $module){
			if( $module['class'] == $class_name ){
				return $module['dir_name'];
			}
		}
		return false;
	}