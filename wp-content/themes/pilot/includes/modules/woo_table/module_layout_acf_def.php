<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => '56e849521e758',
		'name' => 'woo_table_block',
		'label' => 'Woo Table Block',
		'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => 'field_56e8987ac48ad',
				'label' => 'Block Title',
				'name' => 'woo_table_block_title',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_56e8496248737',
				'label' => 'Products',
				'name' => 'woo_table_block_products',
				'type' => 'post_object',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'product',
				),
				'taxonomy' => array (
				),
				'allow_null' => 0,
				'multiple' => 1,
				'return_format' => 'object',
				'ui' => 1,
			),
		),
		'min' => '',
		'max' => '',
	);
?>