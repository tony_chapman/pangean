<?php 
	/**
	 * string	$args['title']
	 * array	$args['products']			 			//  array of products
	 * string	$args['products'][0]['title'] 			//  
	 * string	$args['products'][0]['link'] 			//  
	 * string	$args['products'][0]['weight'] 			//  
	 * string	$args['products'][0]['price'] 			//  
	 */
	global $args; 
?>
<h3><?php echo $args['title']; ?></h3>
<ul class="coffee-products">
	<?php foreach ( $args['products'] as $product ) : ?>
		<li>
		    <div class="product-left">
				<span class="product-name"><?php echo $product['title'] ?></span>
				<a href="<?php echo $product['link']; ?>">Details ></a>
			</div>
			<div class="product-right">
				<span class="size"><?php echo $product['weight']; ?></span>
				<span class="price"><?php echo $product['price']; ?></span>
			</div>
		</li>
	<?php endforeach; ?>
</ul>
