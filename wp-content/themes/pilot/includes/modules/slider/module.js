jQuery(document).ready(function($) {
  $('.block-slider .slider-block').slick({
    centerMode: false,
    slidesToShow: 1,
    easing: 'swing',
    prevArrow: '<div class="slick-prev"></div>',
    nextArrow: '<div class="slick-next"></div>',
    arrows: true,
    autoplay: true,
    responsive: []
  });
});