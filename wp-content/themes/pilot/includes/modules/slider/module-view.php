<?php 
	/**
	 * string	$args['title']
	 * array	$args['slides']
	 * string	$args['slides'][0]['type'] 		// define a type in acf; allows us to use the same slider block for multiple data sources
	 * array	$args['slides'][0]['image'] 	// an acf image array
	 *
	 * slick slider is called from src/js/main.js
	 */

	global $args; 
	$type = $args['slide_type'];
?>
<h3><?php echo $args['title']; ?></h3>
<?php if( count($args['slides']) > 0 ) : ?>

	<div class="slider-block">
		<?php foreach( $args['slides'] as $slide ): ?>

			<?php if( 'slide' == $type ) : ?>
				<div class="slide">
					<div class="bg-image" style="background-image: url(<?php echo $slide['image']['url']; ?>)"></div>
				</div>
			<?php endif; ?>

			<?php if( 'image' == $type ) : ?>
				<div class="image-only">
					<div class="bg-image" style="background-image: url(<?php echo $slide['image']['url']; ?>)"></div>
				</div>
			<?php endif; ?>

		<?php endforeach; ?>
	</div>
<?php endif; ?>