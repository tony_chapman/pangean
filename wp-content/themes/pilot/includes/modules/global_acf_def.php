<?php
if( function_exists('acf_add_options_sub_page') ) {
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Global Modules',
            'menu_title'    => 'Global Modules',
            'menu_slug'     => 'global-modules',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => 3.3,
            'icon_url'      => false
        )
    );    
}
global $pilot;
// add module layout to flexible content 
$global_module_layout = 
				array (
					'key' => '56ea09409c99c',
					'name' => 'global_block',
					'label' => 'Predefined Blocks',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_56ea09452faf8',
							'label' => 'Choose your predefined Block',
							'name' => 'global_block_predefined_block',
							'type' => 'select',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array (
							),
							'default_value' => array (
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'ajax' => 0,
							'placeholder' => '',
							'disabled' => 0,
							'readonly' => 0,
						),
					),
					'min' => '',
					'max' => '',
				);

// gets the predefined blocks and builds a dropdown select of the,
function acf_load_global_blocks( $field ) {
	$field['choices'] = array();
	$rows = get_option('options_content_blocks');
	global $wpdb;
	foreach( $rows as $row ){	
		$array = $wpdb->get_results(
			$wpdb->prepare( 
				"
					SELECT * 
					FROM wp_options
					WHERE option_name LIKE %s
				",
				'options_content_blocks_%_'.$row.'_title'
			)
		);
		$obj = $array[0];
		$field['choices'][$obj->option_value] = $obj->option_value;
	}
	return $field;    
}
add_filter('acf/load_field/name=global_block_predefined_block', 'acf_load_global_blocks');

function build_global_layout(){
	$block_name = get_sub_field('global_block_predefined_block');
	if( have_rows('content_blocks' , 'option' ) ):
		while ( have_rows('content_blocks' , 'option') ): 
			the_row();
			$test_block_type = preg_replace( '/_block/','',get_row_layout());
			$test_block_title = get_sub_field($test_block_type.'_block_title');
			if( $test_block_title == $block_name ){
				$args = call_user_func('build_' . $test_block_type . '_layout');
				$args['global_type'] = preg_replace('/_/','-',$test_block_type);
			}
		endwhile;
	endif;
	return $args;
}

?>