<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_posts_slider_layout(){
		$max_posts = get_sub_field('posts_slider_block_max_number_of_posts');
		$authors = get_sub_field('posts_slider_block_author');
		$author_ids = array();
		if( is_array( $authors) && count( $authors ) > 0 ){
			foreach($authors as $author){
				$author_ids[] = $author['ID'];
			}
		}
		$author_ids = implode(',',$author_ids);
		$query_args = array(
			'author'        =>  $author_ids,
			'orderby'       =>  'post_date',
			'order'         =>  'ASC',
			'posts_per_page' => $max_posts
		);
		$query = new WP_Query( $query_args );
		if($query->have_posts()){
			$slides = array();
			foreach( $query->posts as $slide_post ) {
				$categories = wp_get_post_categories( $slide_post->ID );
				if( is_array( $categories ) ){
					$cat = get_the_category_by_ID( $categories[0] );
					if( 'Uncategorized' == $cat ){
						$cat = "";
					}
				}
				$permalink = get_permalink( $slide_post->ID );
				$author = get_the_author_meta( 'user_nicename', $slide_post->post_author ); 
				$thumbnail = "";
				if( $cat == 'Video' || $cat == 'Messages'){
					if( $video_url = get_field('video_link', $slide_post->ID) ){
						$thumbnail = get_video_thumbnail( $video_url );	
					}
				}
				if( !$thumbnail ){
					$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $slide_post->ID ), 'slide' );
				}
				$slides[] = array(
					'author' => $author,
					'category' => $cat,
					'date' => mysql2date('j M Y', $slide_post->post_date),
					'title' => $slide_post->post_title,
					'thumbnail' => $thumbnail,
					'permalink' => $permalink
				);
			}
		}
		$args = array(
			'title' => get_sub_field('posts_slider_block_title'),
			'slide_type' => 'slide',
			'slides' => $slides
		);
		return $args;
	}

?>