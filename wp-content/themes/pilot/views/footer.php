<footer class="site-footer">
  <div class="site-info">
        
    <div class="footer-left">
      <img src="<?php echo get_template_directory_uri(); ?>/images/Pangean-Footer-Logo.svg" alt="" />
    </div>
    
    <div class="footer-right">
			<?php	if ( has_nav_menu( 'footer' ) ) { 
	      wp_nav_menu( array( 'theme_location' => 'footer') ); 
			} ?>
      <p class="copyright">&copy; <?php echo date("Y"); ?> Pangean Coffee Roasters. All Rights Reserved. <?php printf( esc_html__( 'Design by %1$s', 'pilot' ), '<a href="http://sonderagency.com/">Sonder</a>' ); ?></p>
    </div>
        
	</div><!-- .site-info -->
</footer>