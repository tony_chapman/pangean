<header id="masthead" class="site-header">
	<div class="site-branding">
    <div class="header">
		
        <div class="menu-left">
            <?php if ( has_nav_menu( 'header-left' ) ) {
              wp_nav_menu( array( 'theme_location' => 'header-left') ); 
            } ?>
        </div>
        
        <div class="logo"><a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="" /></a></div>
        
        <div class="main_nav">
        <nav id="site-navigation" class="main-navigation">
		<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'MENU', 'pilot' ); ?></button>
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
	</nav>
        </div>
        </div><!--end header -->
   
		
		<?php //$theme = wp_get_theme(); echo $theme->name; ?>
	</div><!-- .site-branding -->
</header>