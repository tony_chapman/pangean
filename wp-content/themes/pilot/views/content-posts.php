<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="layout-content">
		<div class="post-excerpt-left">
			<?php
				$thumb_id = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
				$thumb_url = $thumb_url_array[0];
			?>
			<img src="<?php echo $thumb_url; ?>">
		</div>
		<div class="post-excerpt-right">
			<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
				<header class="entry-header">
					<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				</header>
			<?php endif; ?>
			<div class="entry-meta">
				<?php the_date('m/d/Y'); ?>
			</div>
			<div class="entry-content">
				<p><?php echo excerpt('50');  //pilot-theme-functions.php ?></p>
				<?php 
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pilot' ),
						'after'  => '</div>',
					) );
				?>
				<div><a class='button' href='<?php the_permalink(); ?>'>Read More</a></div>
			</div><!-- .entry-content -->
			<footer class="entry-footer">
				<?php
					edit_post_link(
						sprintf(
							/* translators: %s: Name of current post */
							esc_html__( 'Edit %s', 'pilot' ),
							the_title( '<span class="screen-reader-text">"', '"</span>', false )
						),
						'<span class="edit-link">',
						'</span>'
					);
				?>
			</footer>
		</div><!-- post-content-right -->
	</div><!-- layout-content -->
</article>