
module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // Create JS Library w/ Bower
        // -----------------------------------
        concat: {
            dev: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/wow/dist/wow.min.js'
                ],
                dest: 'dest/js/lib.min.js'
            }
        },

        // Javascript QA
        // -----------------------------------
        jshint: {
            files: [
                'dest/js/app.js'
            ],
            options: {
                expr: true,
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },

        // Minify Files
        // -----------------------------------
        uglify: {
            dev: {
                options: {
                    beautify: true,
                    compress: true,
                    mangle: false
                },
                files: {
                    'dest/js/app.min.js': [
                        'src/js/*.js',
                        'src/js/**/*.js',
                        'includes/modules/**/*.js',
                    ]
                }
            }
        },

        // Compile Sass
        // -----------------------------------
        sass: {
            options: {
                require: 'susy',
                sourceMap: true,
                outputStyle: 'nested'
            },
            dev: {
                files: {
                    'dest/css/main.min.css': 'src/sass/main.scss',
                    'dest/css/colormaker.min.css': 'src/sass/colormaker.scss'
                }
            }
        },

        // There is no 1 in Browser
        // -----------------------------------
        autoprefixer: {
            options: {
                browsers: ['last 3 versions', 'ie 8', 'ie 9'],
                map: true
            },
            dev: {
                src: 'dest/css/main.min.css',
                dest: 'dest/css/main.min.css'
            }
        },

        // Move fonts and images to dest
        // -----------------------------------
        copy: {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'src/fonts',
                    src: ['**'],
                    dest: 'dest/fonts'
                },{
                    expand: true,
                    cwd: 'src/img',
                    src: ['**'],
                    dest: 'dest/img'
                }]
            }
        },

        // Run shell commands (using for colormaker)
        // -----------------------------------
		shell: {
	        php: {
	            command: 'php -f includes/colormaker/gruntRun.php'
	        }
	    },

        // Work smarter...
        // -----------------------------------
        watch: {
            configFiles: {
                files: ['Gruntfile.js'],
                options: {
                    reload: true
                }
            },
            js: {
                files: [
                    'src/js/*.js',
                    'src/js/**/*.js',
                    'includes/modules/**/*.js'
                ],
                tasks: [
                    'uglify:dev',
                    'jshint'
                ],
                options: {
                    livereload: true
                }
            },
            css: {
                files: [
                    'includes/modules/**/*.scss',
                    'src/sass/*.scss',
                    'src/sass/**/*.scss'
                ],
                tasks: ['sass','shell:php'],
                options: {
                    livereload: true
                }
            },
            assets: {
                files: [
                    'src/img/*',
                    'src/fonts/*',
                ],
                tasks: ['copy'],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-shell'); // for colormaker

    grunt.registerTask('default', [
	    'concat',
        'copy',
        'sass',
        'uglify',
        'jshint',
	    'autoprefixer',
		'shell',	// for colormaker
        'watch'
    ]);

};
